#!/bin/bash

if [ -z "$1" ]
then
	echo "-------------------"
	echo "Usage:"
	echo "./build.sh 'ROM' 'ANDROID_VERSION' 'RECOVERY'"
	echo "Available ROM's: cm, stock, miui"
	echo "Available ANDROID_VERSION's: 4.1; 4.2.2; 4.3.1"
	echo "Available RECOVERY's: cwm, touch, swype (tested only on 4.3.1)"
	echo "if you want to build kernel for 4.2.2 CyanogenMod Based ROM (like PAC) with touch recovery"
	echo "./build.sh cm 4.2.2 touch"
	echo "if you want to build kernel for MIUI with default recovery"
	echo "./build.sh miui 4.1.2 cwm"
	echo "-------------------"
	exit 1
fi

BASEDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
OUTDIR="$BASEDIR/out/$1_$2_$3"
TOOLCHAIN="/home/dinhqui/stone/toolchain/gcc/4.7/arm-eabi/bin/arm-gnueabi-"
KERNEL_VERSION="v3"

case "$4" in
	clean)
		echo -e "\n\n Cleaning Kernel Sources...\n\n"
		make mrproper ARCH=arm CROSS_COMPILE=$TOOLCHAIN
		rm -rf "$BASEDIR/out/"
		ENDTIME=$SECONDS
		echo -e "\n\n Finished in $((ENDTIME-STARTTIME)) Seconds\n\n"
 		;;
	*)
		ROM=$1
		VERSION=$2
		RECOVERY=$3

		if [ "$1" == "stock" ]
		then
		INITRAMFSDIR="$BASEDIR/Ramdisk/Stock/$3"
		else

		rm -rf "$OUTDIR"

		rm -f "$BASEDIR/usr/temp.list"
		cp "$BASEDIR/usr/basic.list" "$BASEDIR/usr/temp.list"
		sed -i 's/!ANDROID_VERSION!/'$2'/g' "$BASEDIR/usr/temp.list"
		sed -i 's/!RECOVERY!/'$3'/g' "$BASEDIR/usr/temp.list"

		INITRAMFSDIR="$BASEDIR/usr/temp.list"
		fi

		echo -e "\n\n Configuring I8160 Kernel...\n\n"
		make Stone_defconfig ARCH=arm CROSS_COMPILE=$TOOLCHAIN

		echo -e "\n\n Compiling I8160 Kernel and Modules... \n\n"
		make -j4 ARCH=arm CROSS_COMPILE=$TOOLCHAIN CONFIG_INITRAMFS_SOURCE=$INITRAMFSDIR

		if [ "$1" == "stock" ]
		then

		mkdir -p $INITRAMFSDIR/lib/modules/
		find . -name "*.ko" -exec cp {} ../$INITRAMFSDIR/lib/modules \;


		echo -e "\n\n Creating zImage...\n\n"
		make ARCH=arm CROSS_COMPILE=$TOOLCHAIN CONFIG_INITRAMFS_SOURCE=$INITRAMFSDIR zImage

		mkdir -p ${OUTDIR}
		cp arch/arm/boot/zImage ${OUTDIR}/kernel.bin

		echo -e "\n\n Pushing Kernel to OUT folder...\n\n"
		pushd ${OUTDIR}
		md5sum -t kernel.bin >> kernel.bin
		mv kernel.bin kernel.bin.md5

		echo -e "\n\n Making flashable zip...\n\n"
		cp flashable/stock/flash_stone.sh $OUTDIR
		cp -avr flashable/META-INF $OUTDIR
		cd $OUTDIR
	    echo -e "\n\n Signing ZIP...\n\n"
		zip -r Stone_$KERNEL_VERSION_stock_$RECOVERY.zip .
	    java -jar ../sign/signapk.jar ../sign/testkey.x509.pem ../sign/testkey.pk8 Stone_$KERNEL_VERSION_stock_$RECOVERY.zip Stone_$KERNEL_VERSION_stock_$RECOVERY\_signed.zip
		cd $BASEDIR

		echo -e "\n\n Cleaning Output Folder...\n\n"
		rm -rf $OUTDIR/system
		rm -rf $OUTDIR/META-INF
		rm $OUTDIR/kernel.bin.md5
		rm $OUTDIR/Stone_$KERNEL_VERSION_stock_$RECOVERY.zip

		else
		echo -e "\n\n Copying Modules to Output Folder...\n\n"

		mkdir -p $OUTDIR/system/lib/modules/
		find . -name "*.ko" -exec cp {} ../$OUTDIR/system/lib/modules \;

		echo -e "\n\n Making flashable ZIP...\n\n"
		cp arch/arm/boot/zImage $OUTDIR/boot.img
		cp -avr flashable/META-INF $OUTDIR
		cd $OUTDIR
		zip -r Stone_$KERNEL_VERSION_$1_$2_$3.zip .
	    echo -e "\n\n Signing ZIP...\n\n"
	    java -jar ../sign/signapk.jar ../sign/testkey.x509.pem ../sign/testkey.pk8 Stone_$KERNEL_VERSION_$1_$2_$3.zip Stone_$KERNEL_VERSION_$1_$2_$3\_signed.zip
		cd $BASEDIR

		echo -e "\n\n Cleaning Output Folder...\n\n"
		rm -rf $OUTDIR/system
		rm $OUTDIR/boot.img
		rm -rf $OUTDIR/META-INF
		rm $OUTDIR/Stone_$KERNEL_VERSION_$1_$2_$3.zip
		fi

        ENDTIME=$SECONDS
        echo -e "\n\n = Finished in $((ENDTIME-STARTTIME)) Seconds =\n\n"
		;;
esac
